    Usage: KeyserState.exe numlock capslock scroll-lock
    
    Each lock parameter must be replaced with one of the following:
       0   Turn off
       1   Turn on
       2   Toggle
       3   Leave it be