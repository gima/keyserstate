/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdio>
#include <windows.h>


enum {
  E_OK, // 0
  ENOT_ENOUGH_PARAMETERS // 1 etc..
};

char gcNumsToggled    = 0;
char gcCapsToggled    = 0;
char gcScrollToggled  = 0;


void PressKey( char rcKey )
{
  keybd_event( rcKey, 0, 0, 0 );
  keybd_event( rcKey, 0, KEYEVENTF_KEYUP, 0 );
}


void ToggleAsNeeded( char rcKey, char rcTurn, char rcToggleIdentifier )
{

  switch ( rcTurn )   // If key is to be ..
  {

    case 0:           // .. Turned off
      if ( rcToggleIdentifier == 1 )  // Check to see if it needs to be turned
      {
        PressKey( rcKey );
      }
      return;

    case 1:           // .. Turned on
      if ( rcToggleIdentifier == 0 )  // Check to see if it needs to be turned
      {
        PressKey( rcKey );
      }
      return;

    case 2:           // .. Toggled
      PressKey( rcKey );

  }

}


int main( int riArgCount, char** rpArgs )
{

  if ( riArgCount < 4  )
  {
    printf( "Usage: KeyserState.exe numlock capslock scroll-lock\n\n" );
    printf( "Each lock parameter must be replaced with one of the following:\n" );
    printf( "   0   Turn off\n" );
    printf( "   1   Turn on\n" );
    printf( "   2   Toggle\n" );
    printf( "   3   Leave it be\n" );

    return ENOT_ENOUGH_PARAMETERS;
  }


  BYTE lcKeys[256];
  GetKeyboardState( lcKeys );


  if ( ( lcKeys[ VK_NUMLOCK ] && 1) == 1 )  // Check to se if numlock is active
  {
    gcNumsToggled = 1;
  }

  if ( ( lcKeys[ VK_CAPITAL ] && 1) == 1 )  // Check to se if capslock is active
  {
    gcCapsToggled = 1;
  }

  if ( ( lcKeys[ VK_SCROLL ] && 1) == 1 )   // Check to se if scroll-lock is active
  {
    gcScrollToggled = 1;
  }

  /*printf( "NL%d>%d, CL%d>%d, SL%d>%d\n",
    NumsToggled,   atoi( rpArgs[1] ),
    CapsToggled,   atoi( rpArgs[2] ),
    ScrollToggled, atoi( rpArgs[3] )
  );*/


  ToggleAsNeeded( VK_NUMLOCK, atoi( rpArgs[1] ), gcNumsToggled );     // Handle Numlock
  ToggleAsNeeded( VK_CAPITAL, atoi( rpArgs[2] ), gcCapsToggled );     // Handle Capslock
  ToggleAsNeeded( VK_SCROLL,  atoi( rpArgs[3] ), gcScrollToggled );   // Handle Scroll-lock


  return E_OK;

}
